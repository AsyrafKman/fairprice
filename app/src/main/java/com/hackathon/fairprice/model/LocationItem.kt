package com.hackathon.fairprice.model

import com.google.gson.annotations.SerializedName

data class LocationItem(

	@field:SerializedName("loc")
	val loc: String? = null
)