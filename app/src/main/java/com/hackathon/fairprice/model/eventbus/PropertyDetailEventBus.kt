package com.hackathon.fairprice.model.eventbus

import com.hackathon.fairprice.model.searchProperty.ListPropertyItem

data class PropertyDetailEventBus(val propertyList: List<ListPropertyItem>)