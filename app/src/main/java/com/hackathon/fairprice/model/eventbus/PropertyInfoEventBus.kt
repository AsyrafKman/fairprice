package com.hackathon.fairprice.model.eventbus

import com.hackathon.fairprice.model.houseType.HouseTypeItem

data class PropertyInfoEventBus(val location: String, val houseTypeItem: HouseTypeItem)