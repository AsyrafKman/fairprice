package com.hackathon.fairprice.model.houseType

import com.google.gson.annotations.SerializedName

data class HouseTypeItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)