package com.hackathon.fairprice.model.searchProperty

import com.google.gson.annotations.SerializedName

data class ListPropertyItem(

	@field:SerializedName("website")
	val website: String? = null,

	@field:SerializedName("furnished_type")
	val furnishedType: String? = null,

	@field:SerializedName("lng")
	val lng: Double? = null,

	@field:SerializedName("town")
	val town: String? = null,

	@field:SerializedName("ownership_type")
	val ownershipType: String? = null,

	@field:SerializedName("property_type_group_name")
	val propertyTypeGroupName: String? = null,

	@field:SerializedName("no_bathrooms")
	val noBathrooms: Double? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("price")
	val price: Double? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("property_type")
	val propertyType: String? = null,

	@field:SerializedName("suburb")
	val suburb: String? = null,

	@field:SerializedName("square_feet")
	val squareFeet: String? = null,

	@field:SerializedName("built_year")
	val builtYear: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("no_bedrooms")
	val noBedrooms: Double? = null,

	@field:SerializedName("property_type_group")
	val propertyTypeGroup: String? = null,

	@field:SerializedName("lat")
	val lat: Double? = null
)