package com.hackathon.fairprice.model.searchProperty

import com.google.gson.annotations.SerializedName

data class Result(

	@field:SerializedName("estimated_price")
	val estimatedPrice: String? = null,

	@field:SerializedName("list_property")
	val listProperty: List<ListPropertyItem>? = null
)