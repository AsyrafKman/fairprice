package com.hackathon.fairprice.retrofit

import android.content.Context
import com.hackathon.fairprice.utilities.AppUtility
import com.hackathon.fairprice.view.listener.BaseListener
import okhttp3.ResponseBody
import retrofit2.Call

class ApiCallClass {

    companion object {

        /**
         * Method to call relevant classes to implement API Call
         *
         * @param progressBar - To indicate if progress has to be shown to the user or not
         * @param context     - Used to access the UI
         * @param request     - Data
         * @param listener    - Action to be performed based on the response
         */
        fun callApi(progressBar: Boolean, context: Context, request: Call<ResponseBody>, listener: BaseListener.OnWebServiceCompleteListener<String>) {
            if (!AppUtility.isInternetAvailable(context)) {
                return
            }

            HttpPostConnection().callWebService(progressBar, context, request, object : HttpPostConnection.OnHttpPostConnectionClassCallbacks<String> {
                override fun onComplete(result: String) {
                    listener.onWebServiceComplete(result)
                }

                override fun onError(error: String) {
                    listener.onWebServiceError(error)
                }
            })
        }
    }
}