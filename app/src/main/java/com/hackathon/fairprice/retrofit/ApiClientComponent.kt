package com.hackathon.fairprice.retrofit

import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(NetModule::class))

interface ApiClientComponent {
    fun exposeRetrofit(): Retrofit
}