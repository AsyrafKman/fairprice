package com.hackathon.fairprice.retrofit

import com.hackathon.fairprice.utilities.constant.HttpConstants
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @GET(HttpConstants.URL_PROPERTY)
    fun getProperty(): Call<ResponseBody>

    @GET(HttpConstants.URL_HOUSE_TYPE)
    fun getHouseType(): Call<ResponseBody>

    @GET(HttpConstants.URL_SEARCH)
    fun searchProperty(@Query("state") state: String,
                          @Query("housetype") houseType: String): Call<ResponseBody>
}