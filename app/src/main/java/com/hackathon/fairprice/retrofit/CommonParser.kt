package com.hackathon.fairprice.retrofit

import android.util.Log
import com.hackathon.fairprice.utilities.constant.ParserConstants
import org.json.JSONException
import org.json.JSONObject

class CommonParser {

    companion object {
        fun getProductDetailRequest(proid: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.PROID, proid)

                Log.d("ProductDetail Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun getSimilarProduct(pagesize: String, page: String, categoryID: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.PAGE_SIZE, pagesize)
                jsonObject.put(ParserConstants.PAGE, page)
                jsonObject.put(ParserConstants.CATEGORYID, categoryID)

                Log.d("ProductDetail Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun addToCartParser(proid: String, quantity: String, options: String, token: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.PRODUCT_ID, proid)
                jsonObject.put(ParserConstants.QUANTITY, quantity)
                jsonObject.put(ParserConstants.OPTIONS, options)
                jsonObject.put(ParserConstants.TOKEN, token)

                Log.d("AddToCart Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun getTokenParser(token: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.TOKEN, token)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun updateCartParser(modelID: String, variantID: String, quantity: String, options: String, token: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.MODEL_ID, modelID)
                jsonObject.put(ParserConstants.VARIANT_ID, variantID)
                jsonObject.put(ParserConstants.QUANTITY, quantity)
                jsonObject.put(ParserConstants.OPTIONS, options)
                jsonObject.put(ParserConstants.TOKEN, token)

                Log.d("UpdateCart Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun deleteCartParser(modelID: String, token: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.MODELID, modelID)
                jsonObject.put(ParserConstants.TOKEN, token)

                Log.d("DeleteCart Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun updateProfileParser(email: String, firstName: String, lastName: String, phone: String, token: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.EMAIL, email)
                jsonObject.put(ParserConstants.FNAME, firstName)
                jsonObject.put(ParserConstants.LNAME, lastName)
                jsonObject.put(ParserConstants.PHONE, phone)
                jsonObject.put(ParserConstants.TOKEN, token)

                Log.d("UpdateCart Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun changePasswordParser(currentPassword: String, newPassword: String, token: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.OLD_PASSWORD, currentPassword)
                jsonObject.put(ParserConstants.NEW_PASSWORD, newPassword)
                jsonObject.put(ParserConstants.TOKEN, token)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun updateAddressParser(addressID: String, firstName: String, lastName: String, email: String, phone: String, address: String, country: String, state: String,
                                postcode: String, city: String, isShip: String, isBill: String, token: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.ADDRESS_ID, addressID)
                jsonObject.put(ParserConstants.FNAME, firstName)
                jsonObject.put(ParserConstants.LNAME, lastName)
                jsonObject.put(ParserConstants.EMAIL, email)
                jsonObject.put(ParserConstants.PHONE, phone)
                jsonObject.put(ParserConstants.ADDRESS, address)
                jsonObject.put(ParserConstants.COUNTRY, country)
                jsonObject.put(ParserConstants.STATE, state)
                jsonObject.put(ParserConstants.POSTCODE, postcode)
                jsonObject.put(ParserConstants.CITY, city)
                jsonObject.put(ParserConstants.isSHIP, isShip)
                jsonObject.put(ParserConstants.isBILL, isBill)
                jsonObject.put(ParserConstants.TOKEN, token)

                Log.d("UpdateAddress Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun createAddressParser(firstName: String, lastName: String, email: String, phone: String, address: String, country: String, state: String,
                                postcode: String, city: String, isShip: String, isBill: String, token: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.FNAME, firstName)
                jsonObject.put(ParserConstants.LNAME, lastName)
                jsonObject.put(ParserConstants.EMAIL, email)
                jsonObject.put(ParserConstants.PHONE, phone)
                jsonObject.put(ParserConstants.ADDRESS, address)
                jsonObject.put(ParserConstants.COUNTRY, country)
                jsonObject.put(ParserConstants.STATE, state)
                jsonObject.put(ParserConstants.POSTCODE, postcode)
                jsonObject.put(ParserConstants.CITY, city)
                jsonObject.put(ParserConstants.isSHIP, isShip)
                jsonObject.put(ParserConstants.isBILL, isBill)
                jsonObject.put(ParserConstants.TOKEN, token)

                Log.d("UpdateAddress Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun deleteAddressParser(addressID: String, token: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.ADDRESS_ID, addressID)
                jsonObject.put(ParserConstants.TOKEN, token)

                Log.d("DeleteCart Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun getProductParser(limit: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.PAGE_SIZE, limit)

                Log.d("Product Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }

        fun getSalesParser(pageSize: String, page: String, sortBy: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.PAGE_SIZE, pageSize)
                jsonObject.put(ParserConstants.PAGE, page)
                jsonObject.put(ParserConstants.SORTBY, sortBy)

                Log.d("Product Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }


        fun searchProductParser(categoryID: String, query: String, minPrice: String, maxPrice: String): String {
            val jsonObject = JSONObject()
            try {
                jsonObject.put(ParserConstants.CATEGORYID, categoryID)
                jsonObject.put(ParserConstants.NAME, query)
                jsonObject.put(ParserConstants.MINPRICE, minPrice)
                jsonObject.put(ParserConstants.MAXPRICE, maxPrice)

                Log.d("SearchProduct Parser", jsonObject.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject.toString()
        }
    }
}