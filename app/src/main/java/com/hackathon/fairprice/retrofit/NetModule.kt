package com.hackathon.fairprice.retrofit

import com.hackathon.fairprice.utilities.App
import com.hackathon.fairprice.utilities.AppPreference
import com.hackathon.fairprice.utilities.constant.HttpConstants
import com.hackathon.fairprice.utilities.constant.PrefConstants
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import okhttp3.logging.HttpLoggingInterceptor

@Module
class NetModule {

    private fun getHttpClient(): OkHttpClient {

        val builder = OkHttpClient.Builder()
        // builder.sslSocketFactory(sslContext.socketFactory, trustManager)

        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)


//        To log the outputs
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BASIC
        builder.addInterceptor(interceptor)

        builder.addInterceptor { chain ->
            val original = chain.request()
            val request: Request

            val appPreference = AppPreference.getInstance(App.mInstance)

//            var token = appPreference.getString(PrefConstants.TOKEN_ID)
            var token = ""
//
//            if (!ValidationUtils.validateString(token)) {
//                token = ""
//            }

            request = original.newBuilder()
                .header("token", token!!)
                .header("Cache-Control", "no-cache")
                .method(original.method(), original.body())
                .build()

            chain.proceed(request)
        }

        return builder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        val baseUrl: String = HttpConstants.BASE_URL

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(getHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}