package com.hackathon.fairprice.ui.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.hackathon.fairprice.R
import com.hackathon.fairprice.ui.fragment.LandingFragment

class LandingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)

        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()
        transaction.add(R.id.main_continer, LandingFragment())
        transaction.commit()
    }

    fun changeFragmentInBackStack(fragment: Fragment?) {
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction().addToBackStack(null)
        transaction.replace(R.id.main_continer, fragment!!)
        transaction.commit()
    }

    fun openBrowser(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }
}