package com.hackathon.fairprice.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.hackathon.fairprice.R
import com.hackathon.fairprice.retrofit.calls.ApiCommon
import com.hackathon.fairprice.ui.activity.LandingActivity
import kotlinx.android.synthetic.main.fragment_landing.*
import kotlinx.android.synthetic.main.toolbar_landing.*

class ABCFragment : Fragment(), View.OnClickListener {
    private lateinit var landingActivity: LandingActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_landing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {
        landingActivity = activity as LandingActivity

    }

    override fun onClick(p0: View?) {
        when (p0) {

        }
    }
}