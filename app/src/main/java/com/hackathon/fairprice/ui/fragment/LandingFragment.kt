package com.hackathon.fairprice.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.hackathon.fairprice.R
import com.hackathon.fairprice.model.eventbus.PropertyInfoEventBus
import com.hackathon.fairprice.model.houseType.HouseTypeItem
import com.hackathon.fairprice.model.houseType.HouseTypeResponse
import com.hackathon.fairprice.retrofit.calls.ApiCommon
import com.hackathon.fairprice.ui.activity.LandingActivity
import com.hackathon.fairprice.utilities.AppUtility
import kotlinx.android.synthetic.main.fragment_landing.*
import kotlinx.android.synthetic.main.toolbar_landing.*
import org.greenrobot.eventbus.EventBus

class LandingFragment : Fragment(), View.OnClickListener, ApiCommon.PropertyCallBack, ApiCommon.HouseTypeCallBack, ApiCommon.SearchPropertyCallBack {
    override fun onSearchPropertySuccess(baseObject: String) {

    }

    private lateinit var landingActivity: LandingActivity
    private var houseType = "Please Select House Type"
    private var location = "Please Select State"
    private var houseTypeList = ArrayList<String>()
    private lateinit var houseTypeItemList: List<HouseTypeItem>
    private var houseTypeSelectedIndex = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_landing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {
//        ApiCommon.getInstance()!!.getProperty(activity!!, this)
        ApiCommon.getInstance()!!.getHouseType(activity!!, this)

        landingActivity = activity as LandingActivity

        val spinnerAdapter = ArrayAdapter(activity!!, R.layout.custom_spinner, resources.getStringArray(R.array.state_malaysia))
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_location.adapter = spinnerAdapter

        sp_location.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                location = resources.getStringArray(R.array.state_malaysia)[p2].toString()

                tv_location.text = location
            }
        }

        tv_submit.setOnClickListener(this)
        iv_back.visibility = View.INVISIBLE
    }

    private fun setupHouseTypeSpinner() {
        val spinnerAdapter = ArrayAdapter(activity!!, R.layout.custom_spinner, houseTypeList)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sp_house_type.adapter = spinnerAdapter

        sp_house_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                houseTypeSelectedIndex = p2
                houseType = houseTypeList[p2]
                tv_house_type.text = houseType
            }
        }
    }

    override fun onClick(p0: View?) {
        when (p0) {
            tv_submit -> {
                if (location == "Please Select State") {
                    Toast.makeText(activity!!, "Please Select a state to continue", Toast.LENGTH_SHORT).show()
                } else {
                    EventBus.getDefault().postSticky(PropertyInfoEventBus(location, houseTypeItemList[houseTypeSelectedIndex]))
                    landingActivity.changeFragmentInBackStack(PropertyInfoFragment())
                }
            }
        }
    }

    override fun onPropertySuccess(baseObject: String) {

    }

    override fun onHouseTypeSuccess(baseObject: String) {
        val houseTypeResponse = AppUtility.gson.fromJson(baseObject, HouseTypeResponse::class.java)
        if (houseTypeResponse != null) {
            houseTypeItemList = houseTypeResponse.result!!

            houseTypeList = ArrayList()
            for (item in houseTypeItemList) {
                houseTypeList.add(item.name!!)
            }

            setupHouseTypeSpinner()
        }
    }
}