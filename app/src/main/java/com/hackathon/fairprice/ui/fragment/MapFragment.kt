package com.hackathon.fairprice.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.hackathon.fairprice.ui.activity.LandingActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import com.hackathon.fairprice.R
import com.hackathon.fairprice.model.eventbus.PropertyDetailEventBus
import com.hackathon.fairprice.model.searchProperty.ListPropertyItem
import com.hackathon.fairprice.view.RecyclerAdapter
import kotlinx.android.synthetic.main.fragment_property_listing.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var landingActivity: LandingActivity
    private lateinit var mainMap: GoogleMap
    private lateinit var propertyList: List<ListPropertyItem>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {
        landingActivity = activity as LandingActivity

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mainMap = googleMap!!

        setUpMap()
    }

    private fun setUpMap() {

        propertyList.forEachIndexed { index, item ->
            val location = LatLng(item.lat!!, item.lng!!)

            var markerColour = BitmapDescriptorFactory.HUE_BLUE
            if(item.price!! > 800000){
                markerColour = BitmapDescriptorFactory.HUE_RED
            }else if (item.price > 500000 && item.price < 800000){
                markerColour = BitmapDescriptorFactory.HUE_ORANGE
            }else if (item.price > 300000 && item.price < 500000){
                markerColour = BitmapDescriptorFactory.HUE_GREEN
            }

            mainMap.addMarker(MarkerOptions().position(location).title(item.name)
                    .snippet(item.town).icon(BitmapDescriptorFactory.defaultMarker(markerColour)))

            if (index == 0) {
                mainMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 12.0f))
            }
        }



        mainMap.isMyLocationEnabled = false
        mainMap.mapType = GoogleMap.MAP_TYPE_NORMAL
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onEvent(propertyDetailEventBus: PropertyDetailEventBus) {
        propertyList = propertyDetailEventBus.propertyList
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onStart() {
        EventBus.getDefault().register(this)
        super.onStart()
    }
}