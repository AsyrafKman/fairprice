package com.hackathon.fairprice.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.hackathon.fairprice.R
import com.hackathon.fairprice.model.eventbus.PropertyDetailEventBus
import com.hackathon.fairprice.model.eventbus.PropertyInfoEventBus
import com.hackathon.fairprice.model.searchProperty.ListPropertyItem
import com.hackathon.fairprice.model.searchProperty.PropertyResponse
import com.hackathon.fairprice.retrofit.calls.ApiCommon
import com.hackathon.fairprice.ui.activity.LandingActivity
import com.hackathon.fairprice.utilities.AppUtility
import com.hackathon.fairprice.utilities.DialogUtils
import kotlinx.android.synthetic.main.fragment_landing.*
import kotlinx.android.synthetic.main.fragment_property_info.*
import kotlinx.android.synthetic.main.toolbar_landing.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class PropertyInfoFragment : Fragment(), View.OnClickListener, ApiCommon.SearchPropertyCallBack {
    private lateinit var landingActivity: LandingActivity
    private lateinit var propertyList: List<ListPropertyItem>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_property_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    private fun initialize() {
        landingActivity = activity as LandingActivity

        tv_yes.setOnClickListener(this)
        tv_no.setOnClickListener(this)
        tv_property_list.setOnClickListener(this)
        iv_back.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0) {
            iv_back -> {
                activity!!.onBackPressed()
            }
            tv_yes -> {
                DialogUtils.updatePriceDialog(landingActivity, activity!!.getString(R.string.app_name), activity!!.getString(R.string.thankyou), 1)
            }
            tv_no -> {
                landingActivity.changeFragmentInBackStack(UpdatePriceFragment())
            }
            tv_property_list -> {
                if (propertyList.isNotEmpty()) {
                    EventBus.getDefault().postSticky(PropertyDetailEventBus(propertyList))
                    landingActivity.changeFragmentInBackStack(PropertyMenuFragment())
                } else {
                    Toast.makeText(activity!!, "There is no property available", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onSearchPropertySuccess(baseObject: String) {
        val propertyResponse = AppUtility.gson.fromJson(baseObject, PropertyResponse::class.java)
        if (propertyResponse != null) {
            propertyList = propertyResponse.result!!.listProperty!!

            if (!propertyResponse.result.estimatedPrice.isNullOrEmpty()) {
                tv_estimated_price.text = propertyResponse.result.estimatedPrice
            } else {
                tv_estimated_price.text = "RM 0.0"
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onEvent(propertyInfoEventBus: PropertyInfoEventBus) {
        var propertyType = propertyInfoEventBus.houseTypeItem.id!!.toLowerCase()

        if (propertyType == "semi-d_villa") {
            propertyType = "semi_d_villa"
        }

        val drawableString = resources.getDrawable(resources.getIdentifier(propertyType, "drawable", context!!.packageName))
        iv_property.setImageDrawable(drawableString)

        ApiCommon.getInstance()!!.searchProperty(activity!!, propertyInfoEventBus.location, propertyInfoEventBus.houseTypeItem.id, this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onStart() {
        EventBus.getDefault().register(this)
        super.onStart()
    }
}