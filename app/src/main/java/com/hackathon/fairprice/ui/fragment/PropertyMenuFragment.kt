package com.hackathon.fairprice.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hackathon.fairprice.R
import com.hackathon.fairprice.ui.activity.LandingActivity
import com.hackathon.fairprice.utilities.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_menu_property.*
import kotlinx.android.synthetic.main.fragment_menu_property.view.*
import kotlinx.android.synthetic.main.toolbar_landing.*
import kotlinx.android.synthetic.main.toolbar_landing.view.*
import androidx.viewpager.widget.ViewPager

class PropertyMenuFragment : Fragment(), View.OnClickListener {
    private lateinit var landingActivity: LandingActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_menu_property, container, false)
        initialize(view)
        return view
    }

    private fun initialize(view: View) {
        landingActivity = activity as LandingActivity

        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(PropertyListFragment(), "Property Listing")
        adapter.addFragment(MapFragment(), "Map")
        adapter.addTab("Property Listing")
        adapter.addTab("Map")
        view.viewPager.adapter = adapter
        view.tabLayout.setupWithViewPager(viewPager)

        view.viewPager.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                if(position == 0){
                    view.tv_property_list.background = resources.getDrawable(R.drawable.custom_curved_box_primary_border)
                    view.tv_property_list.setTextColor(resources.getColor(R.color.colorDarkBlue))

                    view.tv_property_map.background = resources.getDrawable(R.drawable.custom_box_white_border)
                    view.tv_property_map.setTextColor(resources.getColor(R.color.colorBlack))
                }else{
                    view.tv_property_map.background = resources.getDrawable(R.drawable.custom_curved_box_primary_border)
                    view.tv_property_map.setTextColor(resources.getColor(R.color.colorDarkBlue))

                    view.tv_property_list.background = resources.getDrawable(R.drawable.custom_box_white_border)
                    view.tv_property_list.setTextColor(resources.getColor(R.color.colorBlack))
                }
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageScrollStateChanged(state: Int) {}
        })

        view.tv_property_list.setOnClickListener(this)
        view.tv_property_map.setOnClickListener(this)
        view.iv_back.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0) {
            iv_back -> {
                activity!!.onBackPressed()
            }
            tv_property_list -> {
                viewPager.currentItem = 0

                tv_property_list.background = resources.getDrawable(R.drawable.custom_curved_box_primary_border)
                tv_property_list.setTextColor(resources.getColor(R.color.colorDarkBlue))

                tv_property_map.background = resources.getDrawable(R.drawable.custom_box_white_border)
                tv_property_map.setTextColor(resources.getColor(R.color.colorBlack))
            }
            tv_property_map -> {
                viewPager.currentItem = 1

                tv_property_map.background = resources.getDrawable(R.drawable.custom_curved_box_primary_border)
                tv_property_map.setTextColor(resources.getColor(R.color.colorDarkBlue))

                tv_property_list.background = resources.getDrawable(R.drawable.custom_box_white_border)
                tv_property_list.setTextColor(resources.getColor(R.color.colorBlack))
            }
        }
    }
}