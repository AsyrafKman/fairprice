package com.hackathon.fairprice.ui.fragment.authentication

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hackathon.fairprice.R
import com.hackathon.fairprice.ui.activity.AuthenticationActivity
import com.hackathon.fairprice.ui.activity.LandingActivity
import kotlinx.android.synthetic.main.fragment_authentication.*
import kotlinx.android.synthetic.main.fragment_authentication.view.*

class AuthenticationFragment : Fragment(), View.OnClickListener {
    private lateinit var authenticationActivity: AuthenticationActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_authentication, container, false)
        initialize(view)
        return view
    }

    private fun initialize(view: View) {
        authenticationActivity = activity as AuthenticationActivity

        view.tv_login.setOnClickListener(this)
        view.tv_register.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0) {
            tv_login -> {
                startActivity(Intent(activity, LandingActivity::class.java))
                activity!!.finish()
            }
            tv_register -> {
                authenticationActivity.changeFragmentInBackStack(RegisterFragment())
            }
        }
    }
}