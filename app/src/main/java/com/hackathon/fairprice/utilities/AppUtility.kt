package com.hackathon.fairprice.utilities

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Base64
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.gson.Gson
import com.hackathon.fairprice.R
import com.hackathon.fairprice.utilities.constant.PrefConstants
import java.io.ByteArrayOutputStream

class AppUtility {
    companion object {
        val gson = Gson()
        var windowWidth: Int = 0
        var windowsHeight: Int = 0
        var deleteCart = false
        var updateAddress = false
        var openUpdateAddress = false
        var cartVendor = ""

        fun getDimensions(activity: Activity) {
            val displayMetrics = DisplayMetrics()
            activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
            windowsHeight = displayMetrics.heightPixels
            windowWidth = displayMetrics.widthPixels
        }

        /**
         * Method to check if Internet is available
         *
         * @param context - To access the connectivity service
         * @return boolean
         */
        fun isInternetAvailable(context: Context): Boolean {
            var result = false
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val activeNetwork: NetworkInfo?
            activeNetwork = cm.activeNetworkInfo

            if (activeNetwork?.type == ConnectivityManager.TYPE_WIFI) {
                result = true
            } else if (activeNetwork?.type == ConnectivityManager.TYPE_MOBILE) {
                result = true
            } else {
                DialogUtils.singleButtonDialog(context as Activity, context.getString(R.string.no_connection), context.getString(R.string.no_internet), 0)
            }

            return result
        }

        fun isLoggedIn(context: Context): Boolean {
            val appPreference = AppPreference.getInstance(context)

            return appPreference.getBoolean(PrefConstants.IS_LOGGED_IN)
        }

        fun isEmailValid(email: String): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        fun encodeTobase64(image: Bitmap): String {
            val baos = ByteArrayOutputStream()
            image.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val b = baos.toByteArray()

            return Base64.encodeToString(b, Base64.NO_WRAP)
        }

        fun hideKeyboard(view: View) {
            val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}