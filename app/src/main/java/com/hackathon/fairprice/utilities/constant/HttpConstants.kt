package com.hackathon.fairprice.utilities.constant

class HttpConstants {
    companion object {
        const val BASE_URL = "http://104.237.10.212:5000/"
        const val URL_PROPERTY = "property/"
        const val URL_LOCATiON = "location/"
        const val URL_HOUSE_TYPE = "housetype/"
        const val URL_SEARCH = "search?"
    }
}