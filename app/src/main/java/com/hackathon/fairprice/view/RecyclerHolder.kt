package com.hackathon.fairprice.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.hackathon.fairprice.R
import com.hackathon.fairprice.model.searchProperty.ListPropertyItem
import com.hackathon.fairprice.view.listener.RecyclerViewListener
import kotlinx.android.synthetic.main.row_propert_list.view.*

class RecyclerHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    private var listener: RecyclerViewListener? = null

    fun bindView(item: ListPropertyItem, listener: RecyclerViewListener) {
        this.listener = listener

        itemView.tv_property_name.text = item.name
        itemView.tv_property_town.text = "Town : " + item.town
        itemView.tv_property_price.text = "Price : RM " + item.price.toString()

        if(item.website == "propertyguru"){
            itemView.iv_website.setImageDrawable(itemView.resources.getDrawable(R.drawable.ic_propertyguru))
        }else{
            itemView.iv_website.setImageDrawable(itemView.resources.getDrawable(R.drawable.ic_propsocial))
        }


        itemView.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0) {
            itemView -> listener?.onRecyclerClicked(adapterPosition)
        }
    }
}