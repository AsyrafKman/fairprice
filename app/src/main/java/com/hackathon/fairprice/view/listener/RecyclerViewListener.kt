package com.hackathon.fairprice.view.listener

interface RecyclerViewListener {
    fun onRecyclerClicked(position: Int)
}